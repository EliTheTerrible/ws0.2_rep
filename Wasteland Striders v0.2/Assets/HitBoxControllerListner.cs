﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxControllerListner : MonoBehaviour
{
    EnemyHitDetector detector;

    public void OnEnable()
    {
        if(detector == null)
        {
            detector = GetComponentInParent<EnemyHitDetector>();
        }
    }

    public void EnableDamage(EnemyHitDetector.BodyPart _part)
    {
        detector.EnableDamage(_part);
    }
    public void DisableDamage(EnemyHitDetector.BodyPart _part)
    {
        detector.DisableDamage(_part);
    }

    public void SetAttackDamage(float damage)
    {
        detector.SetAttackDamage(damage);
    }
}
