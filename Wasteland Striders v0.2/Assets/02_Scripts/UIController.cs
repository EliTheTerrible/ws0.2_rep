﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour
{
    public TMP_Text currentAmmo;    

    public void Update()
    {
        if (WeaponSwitcher.current != null)
        {
            if (WeaponSwitcher.current.GetCurrentWeapon() != null)
            {
                if(WeaponSwitcher.current.GetCurrentWeapon().behaviour != null)
                    currentAmmo.text = WeaponSwitcher.current.GetCurrentWeapon().behaviour.inMag.ToString();
            }
        }
    }
}
