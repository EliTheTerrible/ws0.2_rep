﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLocationTracker : MonoBehaviour
{
    public LayerMask floolLayer;
    Transform _tranform;
    Dictionary<Transform, SegmentCell> visitedCells = new Dictionary<Transform, SegmentCell>();
    private void Update()
    {
        if (_tranform == null)
            _tranform = transform;
        if (LevelManager.current == null)
            return;
        if (LevelManager.current.area != LevelManager.Area.Level)
            return;

        Ray ray = new Ray(_tranform.position, -_tranform.up);
        RaycastHit hit = new RaycastHit();
        if(Physics.Raycast(ray, out hit, floolLayer))
        {
            if(hit.transform.tag != "Ground")
            {
                return;
            }
            if(!visitedCells.ContainsKey(hit.transform))
            {
                var newVisitedCell = hit.collider.GetComponent<PositionUpdater>().GetCell();
                visitedCells.Add(hit.transform, newVisitedCell);
            }
            var _newCurrentLocation = visitedCells[hit.transform];
            if(PlayerMover.current.currentLocation != _newCurrentLocation)
            {
                if(PlayerMover.current.currentLocation is PremadeRoom)
                {
                    var _premadeRoom = PlayerMover.current.currentLocation as PremadeRoom;
                    _premadeRoom.StopAllCoroutines();
                    _premadeRoom.SetLights(false);
                }
            
                PlayerMover.current.currentLocation = _newCurrentLocation;
                if(_newCurrentLocation is PremadeRoom)
                {
                    var _premadeRoom = _newCurrentLocation as PremadeRoom;
                    _premadeRoom.StopAllCoroutines();
                    _premadeRoom.SetLights(true);
                }
            }

        }
    }
}
