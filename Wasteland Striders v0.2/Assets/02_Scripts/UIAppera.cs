﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIAppera : MonoBehaviour
{
    public bool vendor, start, enlisted;


    [SerializeField] private TMP_Text coustomtext;

    public void OnEnable()
    {
        StartCoroutine(Initialize());
    }

    private IEnumerator Initialize()
    {
        yield return new WaitUntil(() => LevelManager.player != null);
        if (vendor)
            coustomtext = LevelManager.player.vendorText;
        else if (start)
            coustomtext = LevelManager.player.startText;
        else if (enlisted)
            coustomtext = LevelManager.player.startText;
    }

    private void OnDisable()
    {
        if(coustomtext != null)
        {
            coustomtext.enabled = false;        
        }
    }
        
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            coustomtext.enabled = true;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            coustomtext.enabled = false;


        }
    }    
}