﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitZoneTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        LevelManager.current.inExitZone = true;
    }
    private void OnTriggerExit(Collider other)
    {
        LevelManager.current.inExitZone = false;        
    }
}
