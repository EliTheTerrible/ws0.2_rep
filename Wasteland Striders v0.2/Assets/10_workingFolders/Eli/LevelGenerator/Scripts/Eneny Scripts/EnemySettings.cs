﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Enemy Settings")]
public class EnemySettings : ScriptableObject
{
    [Header("Enemy Settings")]
    public float startHealth;
    public float sightRange;
    public float minDetectionRange;
    public float attackRange;
    public float attackRate;
    public float limbsMultiplier;
    public float headMultiplier;
    public float massMultiplier;
    public float DespawnTime;
    [Header("Sight Settings")]
    public float playerSpotsPreSec;
    public float fov;
    public int spotDensity;
    [Header("Patrol Setting")]
    public float roamRange;
    public float stopingDistance;
    [Header("Movment Restrictions")]
    public bool confineToSegment;
    public bool confineToRoom;
    [Header("Navmesh Agent Settings")]
    public float speed;
    public float acceleration;
    public float angularSpeed;
    public float radius;
    public bool autoBraking;
    public bool autoTraverseLinks;
    [Header("Manual Controll")]
    public bool manualControl;

}
