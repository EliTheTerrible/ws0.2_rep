﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapGenerator : MonoBehaviour
{
    public static MapGenerator current;        
    
    [SerializeField] bool moveMap, orientMarker, fogOfWar;
    [SerializeField] float mapScaleFactor = 1f;
    [SerializeField] float wallRelativeTickness = 5f;
    [SerializeField] Sprite wallMapMarker;
    public RectTransform map, playerMarker, mapBG;
    Dictionary<LevelSegment, GameObject> levelMap = new Dictionary<LevelSegment, GameObject>();  // refrence to new map segments gameObjects    
    
        
    
    bool ready = false;
    public void OnEnable()
    {
        if (current != this) {
            if (current != null) {
                Destroy(current.gameObject);
            }
            current = this;
        }                  
    }

    private void Update()
    {
        if(ready) {
            // update mapRelativePosition marker position
            if (moveMap) {
                var _playerPositionX = -LevelManager.player.transform.position.x;
                var _playerPositionY = -LevelManager.player.transform.position.z;
                map.localPosition = new Vector2(_playerPositionX, _playerPositionY) * mapScaleFactor;
                playerMarker.localScale = Vector3.one * mapScaleFactor /2f;
                map.localScale = Vector3.one * mapScaleFactor;
            }
            if(orientMarker)
            {
                var _playerRotation = -LevelManager.player.transform.eulerAngles.y;
                playerMarker.localRotation = Quaternion.Euler(0f,0f,_playerRotation);
            }
        }
        foreach(LevelSegment _segment in levelMap.Keys)
        {
            foreach(SegmentRoom _room in _segment.levelSegmentGenerator.rooms_OLDtoRemove)
            {
                if(_room.visible)
                {
                    _room.ShowOnMap();
                }
            }
        }
    }

    void ClearMap()
    {
        if(levelMap != null) {
            foreach(LevelSegment _segment in levelMap.Keys) {
                Destroy(levelMap[_segment]);
            }
            levelMap = new Dictionary<LevelSegment, GameObject>();
        }
    }
    public void GenerateMap()
    {
        ClearMap();
        map.pivot = Vector2.zero;
        map.sizeDelta = (Vector2)LevelGenerator.current.levelSize * (Vector2)LevelGenerator.current.segmentSize * LevelGenerator.current.cellSizeFactor;
        List<LevelSegment> _segments = LevelGenerator.current.GetSegments();
        foreach(LevelSegment _segment in _segments) {
            
            GameObject _newSegmentMap = new GameObject();
            RectTransform _newSegmentRect = _newSegmentMap.AddComponent<RectTransform>();            
            var _newSegmentSAdjustmentFactor = (Vector2)LevelGenerator.current.segmentSize * LevelGenerator.current.cellSizeFactor;

            _newSegmentRect.SetParent(map);
            _newSegmentMap.name = _segment.name + "_map";
            _newSegmentRect.pivot = Vector2.zero;
            _newSegmentRect.sizeDelta = _newSegmentSAdjustmentFactor;
            _newSegmentRect.localPosition = LevelGenerator.current.GetSegmentAddress(_segment) * _newSegmentSAdjustmentFactor;
            levelMap.Add(_segment, _newSegmentMap);
            CreateSegmentMap(_segment);
        }                
        mapBG.localPosition = Vector3.zero;                
        mapBG.localScale = Vector3.one/mapScaleFactor;
        playerMarker.SetParent(map.parent);
        playerMarker.localPosition = Vector3.zero;       
        ready = true;
    }
    RectTransform CreateNewMapCell(SegmentCell _cellToMap, Vector2 _sizeAdjustmentValue)
    {
        var _segmentToMap = _cellToMap.generator.parentSegment;
        GameObject _newCellGrid = new GameObject();
        RectTransform _newCellGridRect = _newCellGrid.AddComponent<RectTransform>();
        
        _newCellGrid.transform.SetParent(levelMap[_segmentToMap].transform);        
        _newCellGridRect.localPosition = (_cellToMap.coordinates * _sizeAdjustmentValue) + _sizeAdjustmentValue/2f;
        _newCellGridRect.sizeDelta = Vector2.one * _sizeAdjustmentValue;
        _newCellGrid.name = "map_grid_" + _cellToMap.coordinates;

        return _newCellGridRect;
    }
    RectTransform CreateNewWall(Transform _mapCell, MovementDirections dir, Vector2 _sizeAdjustmentValue, string _name, Color _color )
    {        
        GameObject _newWall = new GameObject();
        RectTransform _newWallRect = _newWall.AddComponent<RectTransform>();
        Image _newSegmentMapImage_TEMP = _newWall.AddComponent<Image>();
        _newSegmentMapImage_TEMP.sprite = wallMapMarker;
        _newSegmentMapImage_TEMP.color = _color;
        
        _newWallRect.SetParent(_mapCell);
        _newWallRect.pivot = Vector2.one / 2f;
        _newWallRect.rotation = Quaternion.Euler(0f, 0f, dir.ToRotationUI());
        _newWallRect.sizeDelta = new Vector2(_sizeAdjustmentValue.x, _sizeAdjustmentValue.y / wallRelativeTickness);
        _newWallRect.pivot = new Vector2(0.5f, (-wallRelativeTickness / 2f) + 1);
        _newWallRect.localPosition = Vector2.zero;
                         
        _newWall.name = _name + "_" + dir.ToString();

        return _newWallRect;
    }
    RectTransform CreateNewCorner(Transform _mapCell ,Vector2 _sizeAdjustmentValue, MovementDirections _dir)
    {
        GameObject _newWall = new GameObject();
        RectTransform _newWallRect = _newWall.AddComponent<RectTransform>();
        Image _newSegmentMapImage_TEMP = _newWall.AddComponent<Image>();
        _newSegmentMapImage_TEMP.sprite = wallMapMarker;
        _newSegmentMapImage_TEMP.color = Color.white;

        _newWallRect.SetParent(_mapCell);                
        _newWallRect.sizeDelta = new Vector2(_sizeAdjustmentValue.x/wallRelativeTickness, _sizeAdjustmentValue.x / wallRelativeTickness);
        _newWallRect.pivot = new Vector2((-wallRelativeTickness / 2f) + 1, (-wallRelativeTickness / 2f) + 1);
        
        _newWallRect.localPosition = Vector2.zero;
        _newWallRect.rotation = Quaternion.Euler(0f, 0f, _dir.ToRotationUI());        

        return _newWallRect;
    }
    bool NeedCorner(SegmentCell _cellToCheckForCorners, MovementDirections _dir)
    {
        var _segmentToMap = _cellToCheckForCorners.generator;
        
        // Check if Corner Posible on map cell(no walls or door in dir and next cc dir and neigbhors in those dir are within the segment)
        var _dirEdge = _cellToCheckForCorners.GetEdge(_dir);
        var _nextDirEdge = _cellToCheckForCorners.GetEdge(_dir.GetNextClockwise());
        var _cornerPossible = (_dirEdge is CellPassage) && (_nextDirEdge is CellPassage);                
        
        var _neighbhorCoord1 = _cellToCheckForCorners.coordinates + _dir.ToIntVector2();
        var _neighbhorCoord2 = _cellToCheckForCorners.coordinates + _dir.GetNextClockwise().ToIntVector2();        
        var _neighbhorsValid = _segmentToMap.ContainsCoordinates(_neighbhorCoord1) && _segmentToMap.ContainsCoordinates(_neighbhorCoord2);        
        
        if (!_neighbhorsValid || !_cornerPossible)
            return false;

        // Check if corner is needed
        var _neighborCell1 = _segmentToMap.GetCell(_neighbhorCoord1);
        var _neighborCell2 = _segmentToMap.GetCell(_neighbhorCoord2);
        var _neighbor1_EdgeDir = _dir.GetNextClockwise();
        var _neighbor2_EdgeDir = _dir;
        var _edge1 = _neighborCell1.GetEdge(_neighbor1_EdgeDir);
        var _edge2 = _neighborCell2.GetEdge(_neighbor2_EdgeDir);

        var cond1 = !(_edge1 is CellPassage);
        var cond2 = !(_edge2 is CellPassage);
        var buildCorner = cond1 && cond2;
        return buildCorner;
    }
    void CreateSegmentMap(LevelSegment _segmentToMap)
    {
        var _cellsByRoom = new Dictionary<SegmentRoom, List<GameObject>>();
        var _cellsByGrid = new Dictionary<SegmentCell, GameObject>();
        var _SegmentCells = _segmentToMap.GetCells();
        var _sizeAdjustmentValue = Vector2.one * LevelGenerator.current.cellSizeFactor;
        
        // create map walls
        foreach(SegmentCell _cellToMap in _SegmentCells) {
            var _cellRoom = _cellToMap.room;
            var edges = _cellToMap.GetEdges();
            
            var _newMapCell = CreateNewMapCell(_cellToMap, _sizeAdjustmentValue);            
            if(!_cellsByGrid.ContainsKey(_cellToMap))
                _cellsByGrid.Add(_cellToMap, _newMapCell.gameObject);

            if(!_cellsByRoom.ContainsKey(_cellRoom)) {
                List<GameObject> _newMapElementList = new List<GameObject>();
                _cellsByRoom.Add(_cellRoom, _newMapElementList);
            } 
            
            // Check each direction and create wall as needed;
            foreach (MovementDirections dir in MazeDirections.AllDirections()) {
                var _edgeToCheck = edges[(int)dir];
                if (_edgeToCheck != null) {
                    if(_edgeToCheck is CellWall)
                    {
                        var _newWallName = _cellToMap.name + "_" + edges[(int)dir] + "_wall";
                        var _newMapWall = CreateNewWall(_cellsByGrid[_cellToMap].transform , dir, _sizeAdjustmentValue, _newWallName, Color.white);
                        _cellsByRoom[_cellToMap.room].Add(_newMapWall.gameObject);
                    }
                    else if(_edgeToCheck is CellDoor)
                    {
                        var _newWallName = _cellToMap.name + "_" + edges[(int)dir] + "_door";
                        var _newMapWall = CreateNewWall(_cellsByGrid[_cellToMap].transform, dir, _sizeAdjustmentValue, _newWallName, Color.red);
                        _cellsByRoom[_cellToMap.room].Add(_newMapWall.gameObject);
                    }
                }
            }            
        }                          

        // Check cell for corners                    
        foreach(SegmentCell _cellToCheckForCorners in _SegmentCells) {
            foreach(MovementDirections _dir in MazeDirections.AllDirections()) {                                                                                
                if(NeedCorner(_cellToCheckForCorners, _dir)) {
                    // add and rotate corner to align with _dir at close-right corner                                                     
                    var _newCorner = CreateNewCorner(_cellsByGrid[_cellToCheckForCorners].transform, _sizeAdjustmentValue, _dir);
                    _newCorner.name = _cellToCheckForCorners.name + "_" + _cellToCheckForCorners.GetEdges()[(int)_dir] + "corner_" + (int)_dir;
                    _cellsByRoom[_cellToCheckForCorners.room].Add(_newCorner.gameObject);                        
                }                
            }
        }
        
        // pass map elemnt lists to rooms and hide
        foreach(SegmentRoom _room in _cellsByRoom.Keys) {
            _room.AssignMapObjects(_cellsByRoom[_room]);
            if(fogOfWar) {
                _room.HideOnMap();
            }
        }
    }    
}
