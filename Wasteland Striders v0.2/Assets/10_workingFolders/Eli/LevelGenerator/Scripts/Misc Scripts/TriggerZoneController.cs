﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class TriggerZoneController : MonoBehaviour
{
    public KeyCode triggerKey = KeyCode.Return;
    public string playerTag = "Player";
    public UnityEvent InZoneEvent;    
    public bool playerInZone;


    private void OnTriggerEnter(Collider other)    
    {
        if(other.tag == playerTag)
        {
            playerInZone = true;      
        }
    }

    private void Update()
    {
        if(playerInZone && Input.GetKeyDown(triggerKey))
        {
            InZoneEvent.Invoke();            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == playerTag)
        {
            playerInZone = false;
        }
    }    


    public void TransitionToSurface()
    {
        var _player = new List<GameObject>();
        _player.Add(LevelManager.player.gameObject);
        SceneTransitionController.current.StartTransion("1.Hub", _player);
    }

    public void TransitionToLevel()
    {
        RunManager.current.StartRun();
    }

    public void TransitionToNextLevel()
    {
        RunManager.current.advanceToNextLevel();
    }
}


