﻿using UnityEngine;

public class CellWall : CellEdge {

	[SerializeField] Transform[] wallVariants;
    [SerializeField] bool createProps;
    [SerializeField] Transform props;
	public override void Initialize (SegmentCell cell, SegmentCell otherCell, MovementDirections direction) {
		base.Initialize (cell, otherCell, direction);
        if (wallVariants != null && wallVariants.Length != 0) { 
            wallVariants[Random.Range(0, wallVariants.Length)].gameObject.SetActive(true);
        }
        
        if(createProps) {              
            if(props == null || props.childCount == 0) {
                Debug.LogWarning("No Props Transform assigned, or no props found in hierchy");
                return;
            }
            var _propsCount = props.childCount;
            for(int i = 0; i < _propsCount; ++i) {
                int i_coinToss = Random.Range(0, 5);
                bool _coinToss = false;
                if (i_coinToss == 0) _coinToss = true;                
                props.GetChild(i).gameObject.SetActive(_coinToss);
            }
        }
	}
}