﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PointOfEntry
{
    public Vector2Int address;
    public MovementDirections wall;    
}
public struct RoomPathConnectionInfo
{    
    public bool connected;
    public Dictionary<MovementDirections, List<PointOfEntryInfo>> poeInfo;    
}
public struct PointOfEntryInfo
{
    public PointOfEntry poe;
    public MovementDirections possibleRoomRotaion;
    public List<PathStepInfo> createdPath;
}
public struct PathStepInfo
{
    public Vector2Int coords;
    public List<MovementDirections> availableDirs;
}
public class PremadeRoom : SegmentCell
{
    public bool playerInRoom = false;
    public Vector2Int roomSize;
    public PointOfEntry[] pointsOfEntry;
    public Transform[] enemySpawns;
    public float lightsPowerOnTime;
    public List<Light> localLights;
    public List<UnityEngine.AI.OffMeshLink> entrances = new List<UnityEngine.AI.OffMeshLink>();
    public CurveCollection posiblePowerOnPatterns;
    bool ready = false;
    Dictionary<Light, float> defaultIntensities;
    float powerOnPhase;



    public void SetLights(bool _pir)
    {
        if(!ready)
        {
            defaultIntensities = new Dictionary<Light, float>();
            foreach(Light _light in localLights)
            {
                defaultIntensities.Add(_light, _light.intensity);
            }
            ready = true; ;
        }
        StopAllCoroutines();
        StartCoroutine(LightsState(_pir));
    }
    private IEnumerator LightsState(bool _pir)
    {        
        if (localLights.Count == 0)
        {
            Debug.Log("No Lights Assigned =[");
            yield break;
        }
        if(posiblePowerOnPatterns == null)
        {
            foreach(Light _light in localLights)
            {
                _light.enabled = _pir;
                _light.intensity = _pir ? defaultIntensities[_light] : 0f;
            }
        }
        var powerOnPatterns = new Dictionary<Light, AnimationCurve>();
        for (int i = 0; i < localLights.Count; ++i)
        {
            powerOnPatterns.Add(localLights[i], posiblePowerOnPatterns.curves[Random.Range(0, posiblePowerOnPatterns.curves.Length)]);
        }
        switch (_pir)
        {
            case true:
                {
                    foreach (Light _light in powerOnPatterns.Keys)
                    {
                        _light.enabled = true;
                        _light.intensity = 0;

                    }
                    var _timer = Mathf.InverseLerp(0f, lightsPowerOnTime, powerOnPhase);
                    while (powerOnPhase <= lightsPowerOnTime)
                    {
                        _timer += Time.deltaTime;
                        powerOnPhase = _timer / lightsPowerOnTime;
                        powerOnPhase = Mathf.Clamp(powerOnPhase, 0, 1);
                        foreach(Light _light in powerOnPatterns.Keys)
                        {
                            _light.enabled = _pir;
                            var _lightIntensity = powerOnPatterns[_light].Evaluate(powerOnPhase);
                            _light.intensity = _lightIntensity * defaultIntensities[_light];
                        }                        
                        yield return null;
                    }                    
                    yield break;
                }
            case false:
                {
                    while (powerOnPhase > 0)
                    {
                        powerOnPhase -= Time.deltaTime;
                        powerOnPhase = Mathf.Clamp(powerOnPhase, 0, 1);
                        foreach(Light _light in localLights)
                        {
                            _light.intensity = powerOnPhase * defaultIntensities[_light];
                        }
                        yield return null;
                    }
                    foreach(Light _light in localLights)
                    {
                        _light.enabled = false;
                    }
                    yield break;
                }
        }
        yield return null;
    }
}
