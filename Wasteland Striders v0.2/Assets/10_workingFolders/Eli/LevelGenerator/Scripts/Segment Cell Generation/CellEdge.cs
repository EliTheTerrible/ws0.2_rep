﻿using UnityEngine;

public abstract class CellEdge : MonoBehaviour {

	public SegmentCell cell, otherCell;

	public MovementDirections direction;

    public virtual void Initialize(SegmentCell _cell, SegmentCell _otherCell, MovementDirections _direction)
    {
        // base initialization for a wall 
        cell = _cell;
        otherCell = _otherCell;
        direction = _direction;

        cell.SetEdge(direction, this);
        transform.parent = cell.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = direction.ToRotation();
        transform.localScale = Vector3.one;        
	}	    
}