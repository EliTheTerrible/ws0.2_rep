﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WeaponIdentifier))]
public class WeaponBehaviour : MonoBehaviour
{
    public ParticleSystem muzzleFlash;
    public AudioSource source_Fire;
    public AudioSource source_reload;
    public AudioClip[] sound_shots;
    public AudioClip sound_reload;
    public Transform firePoint;
    public float muzzleVelocity;
    
    public float reloadTime;
    public int magazineCap;
    public int inMag;

    

    public float rof_rpm = 60;
    public bool auto;
    public bool melee;
    
    public LayerMask fireMask = new LayerMask();
    bool projectileSpawned = false;
    bool firing = false;
    bool reloading = false;
    [Range(0f,1f)]public float spreadFactor = 0f;

    WeaponIdentifier identifier;

    private void Start()
    {
        inMag = magazineCap;
    }
    public void OnEnable()
    {
        if (identifier == null)
            identifier = GetComponent<WeaponIdentifier>();
        firing = false;
        reloading = false;
    }
    public void OnDisable()
    {
        PlayerAnimationController.current.ResetToIdle();             
    }

    
    #region Fire Control
    // called each frame mouse1 is held down
    public void HoldTrigger(bool _down)
    {       
        if(!firing)
        {
            if(inMag > 0)
            {
                if(auto)
                {
                    StartCoroutine(FireSequence());        
                }
                else if(_down)
                {
                    StartCoroutine(FireSequence());
                }
            }
            else
            {
                if(_down)
                {
                    StartReload();                        
                }
            }            
        }
    }
    IEnumerator FireSequence()
    {
        firing = true;
        projectileSpawned = false;
        yield return StartCoroutine(TriggerFireAnimation());
        StopFireAnimation();
        firing = false;         
    }

    public void StopFireAnimation()
    {               
        PlayerAnimationController.current.ExitFire();        
    }
    private IEnumerator TriggerFireAnimation()
    {                
        var _spread = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));        
        StartCoroutine(SpawnProjectile());
        PlayerAnimationController.current.SetSpread(_spread, spreadFactor);
        PlayerAnimationController.current.SetRateOfFire(rof_rpm);
        PlayerAnimationController.current.TriggerFire();        
        WaitForSeconds EndOfFire = new WaitForSeconds(1/(rof_rpm/60));
        yield return EndOfFire;
    }
    public IEnumerator SpawnProjectile()
    {
        muzzleFlash.Stop();
        muzzleFlash.Play();
        source_Fire.clip = sound_shots[Random.Range(0, sound_shots.Length)];
        source_Fire.Play();
        yield return new WaitForFixedUpdate();
        if(firePoint != null && !projectileSpawned)
        {
            var _ray = new Ray(PlayerMover.current.fpsCam.transform.position, PlayerMover.current.fpsCam.transform.forward);
            var _hit = new RaycastHit();
            var layerMask = ~((1 << 2) | (1 << 9) | (1 << 10));
            if (Physics.Raycast(_ray.origin, _ray.direction, out _hit, 1000f, layerMask))
            {                
                AmmoPoolingSystem.instance.TriggerFIreLine(_ray.origin, _hit);   
                if(_hit.transform.tag == "Enemy")
                {                    
                    LevelGenerator.current.enemies[_hit.transform].Hit();
                }                
            }            
            --inMag;
            projectileSpawned = true;
        }
    }
    #endregion
    #region Reload Control functions
    public void StartReload()
    {        
        if(inMag < magazineCap && !reloading)
        {
            reloading = true;
            source_reload.clip = sound_reload;
            source_reload.Play();
            StartCoroutine(Reload());
        }
    }
    IEnumerator Reload()
    {        
        StopFireAnimation();
        PlayerAnimationController.current.TriggerReload(reloadTime);
        yield return new WaitForSeconds(reloadTime);
        inMag = magazineCap;
        reloading = false;
        PlayerAnimationController.current.ResetToIdle();
    }
    #endregion
}
