﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireControl : MonoBehaviour
{        
    private void FixedUpdate()
    {
        var currentWeapon = WeaponSwitcher.current.GetCurrentWeapon();
        if (currentWeapon != null)
        {
            //NEED TO FIX!!                        
            if (Input.GetButton("Fire1"))
            {
                var buttonDownFrame = Input.GetButtonDown("Fire1");
                currentWeapon.behaviour.HoldTrigger(buttonDownFrame);                
            }            

            if (Input.GetKeyDown(KeyCode.R))
            {
                currentWeapon.behaviour.StartReload();
            }
        }
        else
        {
            Debug.Log("No Weapon");
        }
    }
}
