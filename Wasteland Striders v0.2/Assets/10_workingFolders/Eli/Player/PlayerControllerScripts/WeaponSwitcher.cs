﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSwitcher : MonoBehaviour
{    
    Dictionary<WeaponAnimationLayer, Transform> weaponAnimationTranformsDictionary = new Dictionary<WeaponAnimationLayer, Transform>();
    Dictionary<string, RectTransform> availableIcons = new Dictionary<string, RectTransform>();
    Dictionary<WeaponCategory, List<WeaponIdentifier>> availableWeapons = new Dictionary<WeaponCategory, List<WeaponIdentifier>>();

    public static WeaponSwitcher current;
    public Transform[] weaponAnimationTransforms;
    public Transform initialWeapons;
    public Transform dropPoint;
    public RectTransform selectedWeaponIconHolder, nextWeaponPreviewIconHolder, previousWeaponPreviewIconHolder;    
    public bool mouseScroll = false;
    public float scrollPreviewDuration = 2f;
    public float scrollActivationDuration = 0.3f;
    public float startOffsetX = 200f;
    public float startOffsetY = 300f;    
    float timer = 0f;
    RectTransform selectedWeaponIcon, nextWeaponPreviewIcon, previousWeaponPreviewIcon;
    WeaponIdentifier currentWeapon = null;
    [SerializeField] bool isScrolling;    
    
    void OnEnable()
    {
        if (current != this)
        {
            if (current != null)
            {                
                return;
            }
            current = this;
        }
        else
        {
            UpdateAnimationController();
            return;
        }

        CreateDictionaries();
        
        var _defaultCategory = WeaponCategory.Pistol;                        
        if (availableWeapons.ContainsKey(_defaultCategory))
        {
            CycleCategoryStart(availableWeapons[_defaultCategory]);
            UpdateSelectedIcon();
            UpdateAnimationController();
        }        
        if (nextWeaponPreviewIconHolder != null)
            nextWeaponPreviewIconHolder.parent.gameObject.SetActive(false);
        if (previousWeaponPreviewIconHolder != null)
            previousWeaponPreviewIconHolder.parent.gameObject.SetActive(false);
    }
    public void PickupWeaopn(WeaponIdentifier _weapon)
    {
        if (weaponAnimationTranformsDictionary.ContainsKey(_weapon.animationLayer))
        {
            _weapon.transform.parent = weaponAnimationTranformsDictionary[_weapon.animationLayer];
            _weapon.transform.localPosition = Vector3.zero;
            _weapon.transform.localRotation = Quaternion.identity;
            _weapon.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogWarning("Unknown Layer!");
            return;
        }
        // check if a key for the current weapon category already exist and add current weapon to it
        AddWeaponToAvailableWeapons(_weapon);

        if(currentWeapon == null)
        {
            currentWeapon = _weapon;
            currentWeapon.gameObject.SetActive(true);
            UpdateAnimationController();
        }
    }

    public void DropWeaopn(WeaponIdentifier weaponToDrop)
    {
        if(weaponToDrop == currentWeapon && weaponToDrop != null)
        {
            weaponToDrop.transform.parent = null;
            weaponToDrop.transform.position = dropPoint.position;
            weaponToDrop.transform.rotation = dropPoint.rotation;
            ScrollToPrevious();
            if(currentWeapon == weaponToDrop)
            {
                currentWeapon = null;
                UpdateAnimationController();
            }
            weaponToDrop.DropWeapon();
            availableWeapons[currentWeapon.category].Remove(weaponToDrop);
        }
    }
    void Update()
    {        
        var _active = initialWeapons != null && !PauseController.current.pause;
        if (_active)
        {
            if(Input.GetKeyDown(KeyCode.Backspace))
            {
                DropWeaopn(currentWeapon);
            }

            if (mouseScroll)
                CheckMousescrollSwitch();

            CheckAlphanumaricSwitch();
        }        
    }

    #region Initialization Methods      
    private void CreateDictionaries()
    {
        // create a refrence dictionary that links between the animated traforms and the available weapon animation layers
        foreach (WeaponAnimationLayer _newLayer in WeaponIdentifier.availableLayers())
        {
            var _tranformIndex = _newLayer.LayerTranformIndex();
            if (_tranformIndex < 0)
                continue;
            weaponAnimationTranformsDictionary.Add(_newLayer, weaponAnimationTransforms[_tranformIndex]);
        }

        // create a dicitionary to sort weapon by category
        foreach (WeaponCategory _cat in AnimationLayerControll.allCategories())
        {
            availableWeapons.Add(_cat, new List<WeaponIdentifier>());
        }
    }

    private void AddWeaponToAvailableWeapons(WeaponIdentifier _weapon)
    {
        var _weaponType = _weapon.category;
        if (availableWeapons.ContainsKey(_weaponType))
        {
            availableWeapons[_weaponType].Add(_weapon);
        }
        // create a new key and add current weapon to it
        else
        {
            var newWeaponList = new List<WeaponIdentifier>();
            availableWeapons.Add(_weaponType, newWeaponList);
            availableWeapons[_weaponType].Add(_weapon);
        }

        // ensure weapon is off
        _weapon.gameObject.SetActive(false);
    }
    #endregion

    
    #region weapon selection and switching mehthods
    IEnumerator ShowScrollPreview()
    {
        nextWeaponPreviewIconHolder.parent.gameObject.SetActive(true);
        previousWeaponPreviewIconHolder.parent.gameObject.SetActive(true);

        var startPositionNext = selectedWeaponIconHolder.parent.position + new Vector3(startOffsetX, 150f, 0f);
        var startPositionPrevious = selectedWeaponIconHolder.parent.position + new Vector3(-300f, -startOffsetY, 0f);
        var endPositionRefrence = selectedWeaponIconHolder.parent.position;

        float _timeElapse = 0f;


        nextWeaponPreviewIconHolder.parent.position = startPositionNext;
        previousWeaponPreviewIconHolder.parent.position = startPositionPrevious;

        while (_timeElapse <= scrollActivationDuration)
        {
            _timeElapse += Time.deltaTime;
            var _nextPrevieUpdatedPosition = Mathf.Lerp(startPositionNext.x, endPositionRefrence.x, _timeElapse / scrollActivationDuration);
            var _previousPrevieUpdatedPosition = Mathf.Lerp(startPositionPrevious.y, endPositionRefrence.y, _timeElapse / scrollActivationDuration);
            nextWeaponPreviewIconHolder.parent.position = new Vector3(_nextPrevieUpdatedPosition, selectedWeaponIconHolder.position.y + 150f, 0f);
            previousWeaponPreviewIconHolder.parent.position = new Vector3(selectedWeaponIconHolder.position.x - 300f, _previousPrevieUpdatedPosition, 0f);
            yield return null;
        }
        while (isScrolling)
        {

            yield return null;
        }
        _timeElapse = 0f;
        while (!isScrolling && _timeElapse <= scrollActivationDuration)
        {
            if (isScrolling)
                _timeElapse = 0f;
            _timeElapse += Time.deltaTime;
            var t = _timeElapse / scrollActivationDuration;
            var _nextPrevieUpdatedPosition = Mathf.Lerp(endPositionRefrence.x, startPositionNext.x, t);
            var _previousPrevieUpdatedPosition = Mathf.Lerp(endPositionRefrence.y, startPositionPrevious.y, t);

            nextWeaponPreviewIconHolder.parent.position = new Vector3(_nextPrevieUpdatedPosition, selectedWeaponIconHolder.position.y + 150f, 0f);
            previousWeaponPreviewIconHolder.parent.position = new Vector3(selectedWeaponIconHolder.position.x - 300f, _previousPrevieUpdatedPosition, 0f);
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    public WeaponIdentifier GetCurrentWeapon()
    {
        return currentWeapon;
    }

    public string GetcurrentWeaponAnimationName()
    {
        if (currentWeapon != null)
            return currentWeapon.animationLayer.ToString();
        else
            return "-1";

    }


    private void CheckMousescrollSwitch()
    {

        var _mouseScrollInput = Input.GetAxisRaw("Mouse ScrollWheel");        
        if (_mouseScrollInput != 0f)
        {
            // Select Next Weapon
            if (_mouseScrollInput > 0)
            {
                ScrollToNext();
            }

            // Select Previous Weapon
            else if (_mouseScrollInput < 0)
            {
                ScrollToPrevious();
            }
            if (!isScrolling)
            {
                StartCoroutine(ShowScrollPreview());
                isScrolling = true;
            }
            timer = 0f;
        }
        else if (isScrolling)
        {
            timer += Time.deltaTime;
            if (timer >= scrollPreviewDuration)
            {
                isScrolling = false;
            }
        }
    }

    private void ScrollToPrevious()
    {
        var _currentWeaponCategoryIndex = currentWeapon == null ? 0 : (int)currentWeapon.animationLayer;
        var _currentWeaponIndexInCategory = currentWeapon == null ? 0 : GetIndexInCategory();
        // Swithc To Last of Previous Category
        if (currentWeapon == null)
        {
            var _previousType = WeaponCategory.Shotgun;
            CycleCategortyLast(availableWeapons[_previousType]);
        }
        else if (_currentWeaponIndexInCategory == 0)
        {
            var _previousTypeIndex = ((int)currentWeapon.animationLayer - 1);
            var _previousType = (WeaponCategory)_previousTypeIndex;
            while (availableWeapons[_previousType].Count == 0)
            {
                _previousTypeIndex = (_previousTypeIndex - 1);
                if (_previousTypeIndex < 0) _previousTypeIndex = availableWeapons.Count - 1;
                _previousType = (WeaponCategory)((_previousTypeIndex) % availableWeapons.Count);
            }

            CycleCategortyLast(availableWeapons[_previousType]);
        }
        // Switch To Previous weapon
        else
        {
            CycleCategoryPrevious(availableWeapons[currentWeapon.category]);
        }
        UpdateScrollPreviewIcons();
        UpdateSelectedIcon();
        UpdateAnimationController();
    }

    private void ScrollToNext()
    {
        var _currentWeaponCategoryIndex = currentWeapon == null ? 0 : (int)currentWeapon.animationLayer;
        var _currentWeaponIndexInCategory = currentWeapon == null ? 0 : GetIndexInCategory();
        // Switch To the start Next Category                
        if (currentWeapon == null)
        {
            var _nextType = WeaponCategory.Melee;
            CycleCategoryStart(availableWeapons[_nextType]);
        }
        else if (_currentWeaponIndexInCategory == availableWeapons[currentWeapon.category].Count - 1)
        {
            var _nextTypeIndex = (_currentWeaponCategoryIndex + 1) % availableWeapons.Count;

            var _nextType = (WeaponCategory)_nextTypeIndex;
            while (availableWeapons[_nextType].Count == 0)
            {
                _nextTypeIndex = (_nextTypeIndex + 1) % availableWeapons.Count;
                _nextType = (WeaponCategory)((_nextTypeIndex) % availableWeapons.Count);
            }
            CycleCategoryStart(availableWeapons[_nextType]);
        }
        // swith to next weapon
        else
        {
            CycleCategoryNext(availableWeapons[currentWeapon.category]);
        }
        UpdateScrollPreviewIcons();
        UpdateSelectedIcon();
        UpdateAnimationController();
    }

    private void CheckAlphanumaricSwitch()
    {
        WeaponCategory _categoryToSelect = new WeaponCategory();
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            _categoryToSelect = WeaponCategory.Unarmed;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _categoryToSelect = WeaponCategory.Melee;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _categoryToSelect = WeaponCategory.Pistol;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _categoryToSelect = WeaponCategory.Rifle;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            _categoryToSelect = WeaponCategory.Shotgun;
        }
        else
            return;
        SelectCategory(_categoryToSelect);
    }

    private void SelectCategory(WeaponCategory _category) {        
        if (availableWeapons.ContainsKey(_category)) {                        
            var weaponsOfType = availableWeapons[_category];
            if (currentWeapon == null) {
                CycleCategoryStart(weaponsOfType);
            }
            else if (currentWeapon.category != _category) {
                CycleCategoryStart(weaponsOfType);
            }
            else {
                CycleCategoryNext(weaponsOfType);
            }
            UpdateSelectedIcon();
            UpdateAnimationController();
        }
    }    
    private int GetIndexInCategory()
    {        
        for (int i = 0; i < availableWeapons[currentWeapon.category].Count; ++i)
            if (availableWeapons[currentWeapon.category][i] == currentWeapon)
                return i;
        return -1;
    }
    private void CycleCategoryNext(List<WeaponIdentifier> _weaponCategory)
    {
        for (int i = 0; i < _weaponCategory.Count; ++i)
        {
            if (_weaponCategory[i] == currentWeapon)
            {
                WeaponIdentifier _nextWeapon;
                if (i == _weaponCategory.Count - 1)
                {
                    _nextWeapon= _weaponCategory[0];

                }
                else _nextWeapon = _weaponCategory[i + 1];
                if (currentWeapon != null && currentWeapon != _nextWeapon)
                {
                    currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
                    currentWeapon.gameObject.SetActive(false);
                    currentWeapon = _nextWeapon;
                    currentWeapon.gameObject.name = currentWeapon.weaponName;
                    currentWeapon.gameObject.SetActive(true);
                }                
                return;
            }
        }
    }
    private void CycleCategoryPrevious(List<WeaponIdentifier> _weaponCategory)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        for (int i = 0; i < _weaponCategory.Count; ++i)
        {
            if (_weaponCategory[i] == currentWeapon)
            {
                if (i == 0)
                {
                    currentWeapon = _weaponCategory[_weaponCategory.Count - 1];
                }
                else currentWeapon = _weaponCategory[i - 1];
                currentWeapon.gameObject.name = currentWeapon.weaponName;
                currentWeapon.gameObject.SetActive(true);
                return;
            }
        }
    }
    public void CycleCategoryStart(List<WeaponIdentifier> _weaponCategory)
    {
        if (_weaponCategory.Count == 0)
        {
            currentWeapon = null;
            return;
        }

        var _weaponToSelect = _weaponCategory[0];        
        if(_weaponToSelect != currentWeapon && _weaponToSelect != null)
        {
            if (currentWeapon != null)
            {
                currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
                currentWeapon.gameObject.SetActive(false);
            }
            currentWeapon = _weaponToSelect;
            currentWeapon.gameObject.name = currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(true);
        }
    }
    public void CycleCategortyLast(List<WeaponIdentifier> _weaponCategory)
    {
        if (_weaponCategory.Count == 0)
        {
            currentWeapon = null;
            return;
        }
        var _weaponToSelect = _weaponCategory[_weaponCategory.Count - 1];
        if (_weaponToSelect != currentWeapon && _weaponToSelect != null)
        {
            if (currentWeapon != null)
            {
                currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
                currentWeapon.gameObject.SetActive(false);
            }
            currentWeapon = _weaponToSelect;
            currentWeapon.gameObject.name = currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(true);
        }
    }
    private void UpdateAnimationController()
    {
        if(currentWeapon == null)
        {
            PlayerAnimationController.current.SelectWeaponLayer(WeaponAnimationLayer.Unarmed);
        }
        else
        {
            PlayerAnimationController.current.SelectWeaponLayer(currentWeapon.animationLayer);
        }
    }
    #endregion

    #region icon refresh mehods
    WeaponIdentifier GetNextWeapon()
    {
        if(currentWeapon == null)
        {            
            return null;
        }
        var _currentWeaponTypeIndex = (int)currentWeapon.category;
        var _currentWeaponTypeCount = availableWeapons[currentWeapon.category].Count - 1;
        var _currentWeaponIndexInList = GetIndexInCategory();

        if (_currentWeaponIndexInList == _currentWeaponTypeCount)
        {
            if (_currentWeaponTypeIndex == (availableWeapons.Count - 1))
            {
                return null;
            }
            var _nextListIndex = _currentWeaponTypeIndex + 1;
            var _nextList = availableWeapons[(WeaponCategory)_nextListIndex];
            while (_nextList.Count == 0)
            {
                _nextListIndex = (_nextListIndex + 1) % availableWeapons.Count;
                _nextList = availableWeapons[(WeaponCategory)_nextListIndex];
            }
            return _nextList[0];
        }
        return availableWeapons[(WeaponCategory)_currentWeaponTypeIndex][_currentWeaponIndexInList + 1];
    }
    WeaponIdentifier GetPreviousWeapon()
    {
        if (currentWeapon == null)
            return availableWeapons[(WeaponCategory)availableWeapons.Count - 1][availableWeapons[(WeaponCategory)availableWeapons.Count - 1].Count - 1];
        var _currentWeaponTypeIndex = (int)currentWeapon.category;
        var _currentWeaponTypeList = availableWeapons[(WeaponCategory)_currentWeaponTypeIndex];
        var _currentWeaponIndexInList = GetIndexInCategory();

        if (_currentWeaponIndexInList == 0)
        {
            if (_currentWeaponTypeIndex == 0)
            {
                return null;
            }
            var _previousTypeIndex = _currentWeaponTypeIndex - 1;
            if (_previousTypeIndex < 0) _previousTypeIndex = availableWeapons.Count - 1;
            var _previousList = availableWeapons[((WeaponCategory)_currentWeaponTypeIndex - 1)];
            while (_previousList.Count == 0)
            {
                _previousTypeIndex = (_previousTypeIndex - 1);
                if (_previousTypeIndex < 0) _previousTypeIndex = availableWeapons.Count - 1;
                _previousList = availableWeapons[(WeaponCategory)_previousTypeIndex];
            }

            if (_previousList.Count == 0)
                return null;
            var _lastOfPreviouseList = _previousList[_previousList.Count - 1];
            return _lastOfPreviouseList;
        }

        return _currentWeaponTypeList[_currentWeaponIndexInList - 1];
    }

    private void UpdateScrollPreviewIcons()
    {
        if (currentWeapon != null)
        {
            if (availableIcons.ContainsKey(currentWeapon.weaponName))
            {
                ShowSelectedIcon(currentWeapon);
            }
            else
            {
                CreateNewIcon(currentWeapon);
                ShowSelectedIcon(currentWeapon);
            }
        }
        else if (selectedWeaponIcon != null)
            selectedWeaponIcon.gameObject.SetActive(false);

        var _nextWeapon = GetNextWeapon();
        var _previousWeapon = GetPreviousWeapon();

        if( _nextWeapon == null)
        {
            ShowNextWeaponPreview(null);
        }
        else if (availableIcons.ContainsKey(_nextWeapon.weaponName))
        {
            ShowNextWeaponPreview(_nextWeapon);
        }
        else
        {
            CreateNewIcon(_nextWeapon);
            ShowNextWeaponPreview(_nextWeapon);
        }

        if(_previousWeapon == null)
        {
            ShowPreviousWeaponPreview(null);
        }
        else if (availableIcons.ContainsKey(_previousWeapon.weaponName))
        {
            ShowPreviousWeaponPreview(_previousWeapon);
        }
        else
        {
            CreateNewIcon(_previousWeapon);
            ShowPreviousWeaponPreview(_previousWeapon);
        }
    }
    private void UpdateSelectedIcon()
    {
        if(currentWeapon == null)
        {            
            return;
        }
        if (availableIcons.ContainsKey(currentWeapon.weaponName))
        {
            ShowSelectedIcon(currentWeapon);
        }
        else
        {
            CreateNewIcon(currentWeapon);
            ShowSelectedIcon(currentWeapon);
        }
        selectedWeaponIcon.gameObject.SetActive(true);
    }
    private void CreateNewIcon(WeaponIdentifier _iconToCreate)
    {
        GameObject newIcon = Instantiate(_iconToCreate.iconPrefab);
        newIcon.name = _iconToCreate.weaponName;
        newIcon.transform.SetParent(selectedWeaponIconHolder);
        newIcon.transform.localPosition = Vector3.zero;
        availableIcons.Add(_iconToCreate.weaponName, (RectTransform)newIcon.transform);
    }

    private void ShowSelectedIcon(WeaponIdentifier _iconToShow)
    {
        if (selectedWeaponIcon != null)
            selectedWeaponIcon.gameObject.SetActive(false);
        selectedWeaponIcon = availableIcons[_iconToShow.weaponName];
        selectedWeaponIcon.SetParent((Transform)selectedWeaponIconHolder);
        selectedWeaponIcon.localPosition = Vector3.zero;
        selectedWeaponIcon.gameObject.SetActive(true);
    }

    private void ShowNextWeaponPreview(WeaponIdentifier _iconToShow)
    {
        if (_iconToShow == null)
        {
            if (nextWeaponPreviewIcon != null)
                nextWeaponPreviewIcon.gameObject.SetActive(false);
            return;
        }
        if (nextWeaponPreviewIcon != null && nextWeaponPreviewIcon != currentWeapon)
            nextWeaponPreviewIcon.gameObject.SetActive(false);
        nextWeaponPreviewIcon = availableIcons[_iconToShow.weaponName];
        nextWeaponPreviewIcon.SetParent((Transform)nextWeaponPreviewIconHolder);
        nextWeaponPreviewIcon.localPosition = Vector3.zero;
        nextWeaponPreviewIcon.gameObject.SetActive(true);
    }
    private void ShowPreviousWeaponPreview(WeaponIdentifier _iconToShow)
    {
        if (_iconToShow == null)
        {
            if (previousWeaponPreviewIcon != null)
                previousWeaponPreviewIcon.gameObject.SetActive(false);
            return;
        }
        if (previousWeaponPreviewIcon != null && previousWeaponPreviewIcon != currentWeapon)
            previousWeaponPreviewIcon.gameObject.SetActive(false);
        previousWeaponPreviewIcon = availableIcons[_iconToShow.weaponName];
        previousWeaponPreviewIcon.SetParent((Transform)previousWeaponPreviewIconHolder);
        previousWeaponPreviewIcon.localPosition = Vector3.zero;
        previousWeaponPreviewIcon.gameObject.SetActive(true);
    }

    #endregion
}
