﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    public static PlayerAnimationController current;
    [Range(0f, 1f)]public float headbobStrenght;
    Animator animator;
    public bool debugControls;
    
    public void OnEnable()
    {
        if(current != this)
        {
            if(current != null)
            {                
                return;
            }
            current = this;
        }                
        if(animator == null) animator = GetComponent<Animator>();
        foreach(string _layerName in AnimationLayerControll.availableAnimationLayers())
        {
            var _layerIndex = animator.GetLayerIndex(_layerName);
            if (_layerIndex < 0)
            {
                Debug.LogWarning("invalid Layer!: cant find " + _layerName + ", check spelling");
            }
        }
        StopAllCoroutines();
        StartCoroutine(AnimationControl());
    }
    #region Fire Control 
    public bool InFireLoop()
    {
        var currentLayerIndex = animator.GetLayerIndex(WeaponSwitcher.current.GetcurrentWeaponAnimationName());
        return animator.GetCurrentAnimatorStateInfo(currentLayerIndex).IsTag("FireLoop");
    }
    public void TriggerFire()
    {        
        animator.SetTrigger("FireDown");               
    }        
    
    
    public void SetRateOfFire(float _rof)
    {
        animator.speed = _rof/60;
    }    
    public void ExitFire()
    {
        animator.ResetTrigger("FireOut");
        if (InFireLoop())
            animator.SetTrigger("FireOut");       
        animator.speed = 1;
    }
    public void SetSpread(Vector2 spread, float factor)
    {
        if(factor > 0)
        {
            animator.SetBool("Spread", true);
            animator.SetFloat("Spread_x", spread.normalized.x * factor);
            animator.SetFloat("Spread_y", spread.normalized.y * factor);
        }
        else
        {
            animator.SetBool("Spread", false);
        }

    }
    #endregion
    #region Reload Control
    public void TriggerReload(float _realoadTime)
    {             
        animator.SetTrigger("Reloading");
        animator.speed = _realoadTime > 0 ? 1 / _realoadTime : 1f;     
    }        
    #endregion
    #region State Control
    public void ResetToIdle()
    {        
        ExitFire();                                
        ResetAnimatorSpeed();
        var currentLayerIndex = animator.GetLayerIndex(WeaponSwitcher.current.GetcurrentWeaponAnimationName());
        animator.Play("Idle", currentLayerIndex);
    }
    // called from animation, will reset animator speed to 1 if state somehow returned to idle with offset speed
    public void ResetAnimatorSpeed()
    {
        animator.speed = 1;
    }
    
    public float GetAnimationProgress()
    {
        var currentLayerIndex = animator.GetLayerIndex(WeaponSwitcher.current.GetcurrentWeaponAnimationName());
        return animator.GetCurrentAnimatorStateInfo(currentLayerIndex).normalizedTime;
    } 
    private IEnumerator AnimationControl()
    {
        while(enabled)
        {
            while (PlayerMover.current == null || PauseController.current == null)
                yield return null;
            while (!PauseController.current.pause)
            {                
                var _playerVelocity = PlayerMover.current.Get2DVelocity();                                

                animator.SetFloat("V_mag", Mathf.Clamp((_playerVelocity.magnitude * headbobStrenght), -1, 1));
                animator.SetFloat("V_side", Mathf.Clamp(_playerVelocity.x, -1, 1));
                animator.SetFloat("V_forward", Mathf.Clamp(_playerVelocity.y, -1, 1));                
                yield return null;
            }
            yield return null;
        }
    }
    public void SelectWeaponLayer(WeaponAnimationLayer _layerToSelect)
    {        
        var _layerToSelectIndex = animator.GetLayerIndex(_layerToSelect.ToString());
        foreach(WeaponAnimationLayer _layerToCheck in WeaponIdentifier.availableLayers())
        {
            var _layerToCheckIndex = animator.GetLayerIndex(_layerToCheck.ToString());
            if ((_layerToCheckIndex == _layerToSelectIndex) && _layerToCheckIndex != 0)
                animator.SetLayerWeight(_layerToCheckIndex, 1f);
            else
                animator.SetLayerWeight(_layerToCheckIndex, 0f);
        }
    }
    #endregion
}
