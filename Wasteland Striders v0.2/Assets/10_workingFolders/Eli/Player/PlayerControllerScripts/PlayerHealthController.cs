﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class PlayerHealthController : MonoBehaviour
{
    public static PlayerHealthController current;
    
    float health;
    [SerializeField] float cooldownFactor = 1f;
    [SerializeField] TMP_Text dbug_currentHealh;
    [SerializeField] RectTransform healthBarMask;
    float startingWidth = 0f;
    
    [SerializeField] float startHealth = 100f;
    private bool takingDamage;
    
    private void OnEnable()
    {
        if(current != this)
        {
            if(current != null)
            {
                current.enabled = false;
            }
            current = this;
            Initialize();
        }
    }

    public void Initialize()
    {
        health = startHealth;
        if (dbug_currentHealh != null)
            dbug_currentHealh.text = health.ToString("F1");
        
        if(healthBarMask != null)
            startingWidth = healthBarMask.sizeDelta.x;
        startingWidth = healthBarMask.sizeDelta.x;
        
        if (healthBarMask != null)
        {            
            Vector2 newSizeDelta = new Vector2(startingWidth, healthBarMask.sizeDelta.y);
            healthBarMask.sizeDelta = newSizeDelta;
        }
    }

    public void HitPlayer(float damage)
    {
        if(!takingDamage && damage != 0f)
        {            
            takingDamage = true;
            StartCoroutine(DamageCooldown(damage));            
        }
    }

    IEnumerator DamageCooldown(float damage)
    {
        yield return new WaitForEndOfFrame();
        if (cooldownFactor == 0)
            cooldownFactor = 1f;
        var duration = (damage / 10f) / cooldownFactor;
        var t = 0f;
        var startingHealth = health;
        Debug.Log(duration + " " + health + " " + t + " " + startingHealth);
        while(t < duration)
        {            
            if(health > 0)
            {
                t += Time.deltaTime;
                var phase = t / duration;
                Debug.Log("Health " + health);
                if(dbug_currentHealh != null)
                    dbug_currentHealh.text = health.ToString("F1");

                health = startingHealth - damage * phase;
                Debug.Log("Health " + health);
                if (healthBarMask != null)
                {
                    var healthbarMaskPhase = startingWidth * health / startHealth;
                    Vector2 newSizeDelta = new Vector2(healthbarMaskPhase, healthBarMask.sizeDelta.y);
                    healthBarMask.sizeDelta = newSizeDelta;
                }                
            }
            yield return new WaitForEndOfFrame();
        }
        health = startingHealth - damage;
        if(health < 0)
        {
            health = 0f;
        }

        if (healthBarMask != null)
        {
            var healthbarMaskPhase = startingWidth * health / startHealth;
            Vector2 newSizeDelta = new Vector2(healthbarMaskPhase, healthBarMask.sizeDelta.y);
            healthBarMask.sizeDelta = newSizeDelta;
        }

        if (dbug_currentHealh != null)
            dbug_currentHealh.text = health.ToString("F1");        
        
        takingDamage = false;
    }
}
