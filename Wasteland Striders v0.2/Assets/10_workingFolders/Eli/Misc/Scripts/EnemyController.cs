﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour
{
    public enum State { idle, patroling, chasing }    

    [SerializeField] Transform walkpoint_Marker;
    [SerializeField] Transform player;
    [SerializeField] Transform _transform;

    [Header("Local Refrencess")]
    [SerializeField] EnemySettings settings;
    [SerializeField] NavMeshAgent agent;
    [SerializeField] EnemyHealthController healthController;
    [SerializeField] EnemyHitDetector hitDetector;
    [SerializeField] Transform eyes;
    [SerializeField] LayerMask GroundM, PlayerM;
    [SerializeField] GameObject Projectile;

    [Header("Exposed Behaviour state")]
    [SerializeField] bool ready = false;
    [SerializeField] State currentState;
    [SerializeField] NavMeshPathStatus pathState;    
    [SerializeField] bool playerInSightRange;
    [SerializeField] bool playerSpoted;
    [SerializeField] bool playerInAttackRange;
    [SerializeField] bool attacking;
    [SerializeField] bool alreadyAttacked;
    [SerializeField] float fowardForce;
    [SerializeField] float upwardForce;
    [SerializeField] Vector3 walkPoint;
    [SerializeField] List<Vector3> sightData;
    [SerializeField] bool linking = false;

    [Header("Animation Settings")]
    public Animator animator;
    [Range(-1f, 1f)] public float v_right;
    [Range(-1f, 3f)] public float v_forward;

    public EnemySettings GetSettings()
    {
        return settings;
    }
    public void Spawn(EnemySettings _settings)
    {
        if (walkpoint_Marker != null)
        {
            walkpoint_Marker.parent = null;
            walkpoint_Marker.gameObject.SetActive(true);
        }
        StopAllCoroutines();
        settings = _settings;
        StartCoroutine(Initialize());
    }
    public void DisableControllers()
    {        
        agent.enabled = false;
        StartCoroutine(hitDetector.Despawn());
    }
    IEnumerator Initialize()
    {
        ready = false;
        currentState = State.idle;

        yield return new WaitUntil(() => LevelManager.current != null);

        if (!LevelManager.current.activeEnemies.ContainsKey(gameObject))
            LevelManager.current.activeEnemies.Add(gameObject, this);

        if (_transform == null)
            _transform = transform;

        if (player == null)
            player = LevelManager.player.transform;

        if (healthController == null)
            healthController = gameObject.AddComponent<EnemyHealthController>();

        if (!TryGetComponent<EnemyHitDetector>(out hitDetector))
        {
            Debug.Log("no hit detector found!");
            yield break;
        }
        yield return new WaitUntil(() => LevelGenerator.current != null);
        if (!TryGetComponent<NavMeshAgent>(out agent))
            agent = gameObject.AddComponent<NavMeshAgent>();
        if (settings != null)
        {
            healthController.Initialize(settings);
            hitDetector.Initialize(settings);
            agent.speed = settings.speed;
            agent.acceleration = settings.acceleration;
            agent.angularSpeed = settings.angularSpeed;
            agent.radius = settings.radius;
            agent.autoBraking = settings.autoBraking;
            agent.autoTraverseOffMeshLink = false;            
            StartCoroutine(UpdateAgentSettings());
            StartCoroutine(SpotPlayer());
            StartCoroutine(Patrol()); 
            ready = true;
        }
        yield return null;
    }

    private void Update()
    {
        #region failsafes and manual control
        if (settings.manualControl) {
            animator.SetFloat("v_forward", v_forward);
            animator.SetFloat("v_right", v_right);
            return;
        }

        if (!ready || settings == null) return;

        if (!LevelGenerator.current.navmeshReady)
            return;
        if (agent == null)
            return;
        if (agent.enabled == false)
            return;
        #endregion

        
        HandleLink();            
        if (walkpoint_Marker != null)
            walkpoint_Marker.position = walkPoint;
        playerInAttackRange = Vector3.Distance(_transform.position, player.position) <= settings.attackRange;        
        pathState = agent.pathStatus;       
        UpdateAnimatorVelocity();
    }  
    private void HandleLink()
    {
        if (agent.isOnOffMeshLink)
        {
            if (!linking)
            {
                linking = true;
                StartCoroutine(MoveAcrossNavMeshLink());
            }            
        }       
        else
        {
            linking = false;
            agent.speed = settings.speed;
        }
    }
    
    IEnumerator MoveAcrossNavMeshLink()
    {               
        var _speed = agent.velocity.magnitude;
        if(_speed < settings.speed/4f)
        {
            _speed = settings.speed / 2f;
        }
        agent.speed = _speed;
        agent.isStopped = true;
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        (agent.navMeshOwner as NavMeshLink).autoUpdate = true;
        Vector3 startPos = _transform.position;
        Vector3 endPos = data.endPos;                
        
        float duration = (endPos - startPos).magnitude / _speed;
        float t = 0f;
        while (t <= duration)
        {                    
            if (healthController.GetHealth() <= 0f)
                yield break;
            var dX = Vector3.Lerp(startPos, endPos, t/duration);                        
            agent.Move(dX - _transform.position);
            t += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }        
        if (healthController.GetHealth() <= 0f)
            yield break;        
        agent.CompleteOffMeshLink();        
        agent.destination = walkPoint;
        agent.isStopped = false;
        linking = false;                   
    }
    
    void UpdateAnimatorVelocity()
    {        
        var relativeVelocity = _transform.InverseTransformVector(agent.velocity);
        if (!linking)
        {
            animator.SetFloat("v_forward", relativeVelocity.z);
            animator.SetFloat("v_right", relativeVelocity.x);
        }
        else if (linking)
        {
            animator.SetFloat("v_forward", agent.speed);
            animator.SetFloat("v_right", 0);
        }        
    }
    void Chasing()
    {
        agent.SetDestination(player.position);
    }
    void Attacking()
    {
        //agent.SetDestination(transform.position);
        agent.isStopped = true;
        transform.LookAt(player);

        // Attack Specification
        Rigidbody rb = Instantiate(Projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();

        rb.AddForce(transform.forward * fowardForce, ForceMode.Impulse);
        rb.AddForce(transform.up * upwardForce, ForceMode.Impulse);

        if (!alreadyAttacked) {
            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), settings.attackRate);
        }
    }
    void ResetAttack()
    {
        alreadyAttacked = false;
    }
    public void TakeDamage(float _damage, EnemyHitDetector.BodyPart _part)
    {
        switch (_part)
        {
            case EnemyHitDetector.BodyPart.Torso:
                {
                    healthController.TakeTorsoDamage(_damage);
                    animator.SetTrigger("hit_torso");
                    break;
                }
            case EnemyHitDetector.BodyPart.LeftArm:
                {
                    healthController.TakeLimbDamage(_damage);
                    animator.SetTrigger("hit_leftArm");
                    break;
                }
            case EnemyHitDetector.BodyPart.RightArm:
                {
                    healthController.TakeLimbDamage(_damage);
                    animator.SetTrigger("hit_rightArm");
                    break;
                }
            case EnemyHitDetector.BodyPart.LeftLeg:
                {
                    healthController.TakeLimbDamage(_damage);
                    animator.SetTrigger("hit_leftLeg");
                    break;
                }
            case EnemyHitDetector.BodyPart.RightLeg:
                {
                    healthController.TakeLimbDamage(_damage);
                    animator.SetTrigger("hit_rightLeg");
                    break;
                }
            case EnemyHitDetector.BodyPart.Head:
                {
                    healthController.TakeHeadDamage(10f);
                    break;
                }
        }
    }            
    IEnumerator Patrol()
    {
        while (enabled)
        {
            if (currentState != State.chasing)
            {
                if (Vector3.Distance(_transform.position, walkPoint) <= settings.stopingDistance)
                {
                    currentState = State.idle;
                }
            }
            while (currentState == State.idle)
            {
                yield return null;
                NavMeshHit navHit;                
                var walkpointFound = false;
                while (!walkpointFound)
                {
                    var _randomPoint = _transform.position + new Vector3(Random.Range(-settings.roamRange, settings.roamRange), 0f, Random.Range(-settings.roamRange, settings.roamRange));
                    if (NavMesh.SamplePosition((_randomPoint), out navHit, 1f, NavMesh.AllAreas))
                    {                            
                        currentState = State.patroling;
                        walkPoint = navHit.position;
                        agent.SetDestination(walkPoint);
                        walkpointFound = true;
                    }                    
                    yield return null;                    
                }
            }
            while (currentState == State.chasing)
            {
                yield return null;
                if (agent.isActiveAndEnabled)
                {
                    agent.stoppingDistance = settings.stopingDistance;
                    walkPoint = LevelManager.player.transform.position;
                    agent.SetDestination(walkPoint);
                    if (playerInAttackRange)
                    {                        
                        animator.SetInteger("Attack", Random.Range(0, 100));
                        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(3).IsTag("enemyAttack"));
                        yield return new WaitUntil(() => !animator.GetCurrentAnimatorStateInfo(3).IsTag("enemyAttack"));
                        animator.SetInteger("Attack", 0);                        
                    }
                    if (Vector3.Distance(_transform.position, LevelManager.player.transform.position) > settings.sightRange)
                        currentState = State.idle;
                }
            }
            if (agent.pathStatus == NavMeshPathStatus.PathPartial && currentState != State.chasing)
                currentState = State.idle;
            yield return null;
        }
    }
    IEnumerator SpotPlayer()
    {
        while (enabled) {
            yield return new WaitUntil(() => LevelGenerator.current != null);
            if (!playerSpoted) {
                for (int i = 0; i < settings.spotDensity; ++i) {
                    var _realativeDir = (-settings.fov / 2) + (i * (settings.fov / settings.spotDensity));
                    eyes.localRotation = Quaternion.Euler(0f, _realativeDir, 0f);
                    RaycastHit _spotChek = new RaycastHit();
                    if (Physics.Raycast(eyes.position, eyes.forward, out _spotChek, settings.sightRange)) {
                        Debug.DrawRay(eyes.position, eyes.forward * _spotChek.distance, Color.red);
                        if (_spotChek.transform.tag == "Player") {
                            playerSpoted = true;
                            currentState = State.chasing;
                        }
                    }
                }
            }
            yield return new WaitForSeconds(1 / settings.playerSpotsPreSec);
        }
    }
    IEnumerator UpdateAgentSettings()
    {
        while (enabled) {
            yield return new WaitUntil(() => LevelGenerator.current != null);
            if (agent.speed != settings.speed) {
                agent.speed = settings.speed;
            }
            if (agent.acceleration != settings.acceleration) {
                agent.acceleration = settings.acceleration;
            }
            if (agent.angularSpeed != settings.angularSpeed) {
                agent.angularSpeed = settings.angularSpeed;
            }
            if (agent.radius != settings.radius) {
                agent.radius = settings.radius;
            }
            if (agent.autoBraking != settings.autoBraking) {
                agent.autoBraking = settings.autoBraking;
            }
            if (agent.autoTraverseOffMeshLink != settings.autoTraverseLinks)
                agent.autoTraverseOffMeshLink = settings.autoTraverseLinks;
            yield return null;
        }
    }    
}
