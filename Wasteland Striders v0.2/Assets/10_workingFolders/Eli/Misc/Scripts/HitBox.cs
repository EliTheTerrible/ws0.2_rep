﻿using UnityEngine;
using System.Collections;

public class HitBox : MonoBehaviour
{   
    public EnemyHitDetector.BodyPart part;
    public Collider collider;
    EnemyHitDetector hitDetector;
    public bool canDamage = false;
    bool hasDamaged = false;
    float currentAttackDamage = 0;
    public void Initialize(EnemyHitDetector _parentHD)
    {
        hitDetector = _parentHD;
        LevelGenerator.current.enemies.Add(transform, this);
        if (collider == null)
            collider = GetComponent<Collider>();
    }

    public void Hit()
    {        
        hitDetector.RecieveDamgeIn(part);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(canDamage && !hasDamaged)
        {
            if (collision.gameObject.tag == "Player")
            {
                PlayerHealthController.current.HitPlayer(currentAttackDamage);
                hasDamaged = true;
            }
        }
    }
    public void StartAttack(float damage)
    {
        canDamage = true;
        hasDamaged = false;
        currentAttackDamage = damage;
    }
    public void EndAttack()
    {
        canDamage = false;
        hasDamaged = false;
        currentAttackDamage = 0f;
    }
}
