﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitDetector : MonoBehaviour
{
    EnemyController controller;
    public enum BodyPart { Head, Torso, LeftLeg, RightLeg, LeftArm, RightArm }
    [SerializeField] List<Collider> torso, leftLeg, rightLeg, leftArm, rightArm, head;
    [SerializeField] WaitForSeconds despawnDelay;
    
    List<HitBox> _hitboxControllers = new List<HitBox>();

    public void Initialize(EnemySettings _settings)
    {        
        despawnDelay = new WaitForSeconds(_settings.DespawnTime);
        List<Collider> _hitboxColliders = new List<Collider>();
        if (controller == null)
            controller = GetComponent<EnemyController>();
        _hitboxColliders.AddRange(head);
        _hitboxColliders.AddRange(torso);
        _hitboxColliders.AddRange(leftArm);
        _hitboxColliders.AddRange(rightArm);
        _hitboxColliders.AddRange(leftLeg);
        _hitboxColliders.AddRange(rightLeg);
        foreach (Collider hitbox in _hitboxColliders)
        {
            var newHitBox = hitbox.gameObject.AddComponent<HitBox>();
            _hitboxControllers.Add(newHitBox);
            newHitBox.Initialize(this);
            if (torso.Contains(hitbox))
            {
                newHitBox.part = BodyPart.Torso;
            }
            else if (leftArm.Contains(hitbox))
            {
                newHitBox.part = BodyPart.LeftArm;
            }
            else if (rightArm.Contains(hitbox))
            {
                newHitBox.part = BodyPart.RightArm;
            }
            else if (leftLeg.Contains(hitbox))
            {
                newHitBox.part = BodyPart.LeftLeg;
            }
            else if (rightLeg.Contains(hitbox))
            {
                newHitBox.part = BodyPart.RightLeg;
            }
            else if (head.Contains(hitbox))
            {
                newHitBox.part = BodyPart.Head;
            }
        }
    }

    public void RecieveDamgeIn(BodyPart _hitPart)
    {
        controller.TakeDamage(10f, _hitPart);
        
    }
    public void EnableDamage(BodyPart _partDamaging)
    {
        foreach(HitBox _hitbox in _hitboxControllers)
        {
            if(_hitbox.part == _partDamaging)
            {                
                _hitbox.StartAttack(nextAttackDamage);
            }
        }
    }

    float nextAttackDamage = 0f;
    public void SetAttackDamage(float damage)
    {
        nextAttackDamage = damage;
    }
    public void DisableDamage(BodyPart _partDamaging)
    {
        foreach (HitBox _hitbox in _hitboxControllers)
        {
            if (_hitbox.part == _partDamaging)
            {
                _hitbox.EndAttack();
            }
        }
    }

    public IEnumerator Despawn()
    {
        yield return despawnDelay;
        foreach(HitBox hitbox in _hitboxControllers) 
            hitbox.collider.enabled = false;
        yield return despawnDelay;
        gameObject.SetActive(false);
    }
}
