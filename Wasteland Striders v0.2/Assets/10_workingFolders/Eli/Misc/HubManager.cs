﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubManager : LevelManager
{
    public Transform FromHubSpawn;
    public Transform FromLevelSpawn;

    bool inReturnToSurfaceZone;
    public override void OnEnable()
    {
        base.OnEnable();
        StartCoroutine(InitializePlayer());
    }

    IEnumerator InitializePlayer()
    {
        yield return new WaitUntil(()=> player!=null);
                    
        
        player.gameObject.SetActive(true);
        if(RunManager.current.state == RunManager.GameState.run)
        {
            player.transform.position = FromLevelSpawn.position;
            player.transform.rotation = FromLevelSpawn.rotation;        

        }
        else if( RunManager.current.state == RunManager.GameState.surface)
        {
            player.transform.position = FromHubSpawn.position;
            player.transform.rotation = FromHubSpawn.rotation;
        }
        RunManager.current.state = RunManager.GameState.surface;
        yield return null;
    }

    public override void Update()
    {
        if (player == null)
            Debug.Log("No Player");
        switch (area)
        {

            case Area.Shop:
            {                
                if (inReturnToSurfaceZone && Input.GetKeyDown(KeyCode.Return))
                {
                        var _player = new List<GameObject>();
                        _player.Add(player.gameObject);                        
                        SceneTransitionController.current.StartTransion("1.Hub", _player);
                }
                break;
            }
            case Area.Surface:
            {
                if (inExitZone && Input.GetKeyDown(KeyCode.Return))
                {
                    var _player = new List<GameObject>();
                    _player.Add(player.gameObject);
                    SceneTransitionController.current.StartTransion("2.shop_Level", _player);
                }
                break;
            }
        }
    }
}
