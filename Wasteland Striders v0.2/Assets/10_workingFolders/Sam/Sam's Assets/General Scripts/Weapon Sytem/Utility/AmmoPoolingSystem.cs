﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPoolingSystem : MonoBehaviour
{    
    public static AmmoPoolingSystem instance;
    public List<Bullet> akBullets;
    public List<LineRenderer> fireLines;
    public List<SpriteRenderer> hitImages;
    public List<ParticleSystem> bloodEffect, sparksEffects;
    
    public static AmmoPoolingSystem Instance { get { return instance; } }
    public  GameObject bulletPrefab;
    public  GameObject firelinePrefab;
    public GameObject hitImagePrefab;
    public GameObject ps_Blood, ps_Sparks;
    public float firelineDrawDuration;
    public float hitImageDuration;

    public int initialPoolSize = 20;
    void Awake()
    {
        if(instance != this) {
            if(instance != null) {                
                return;
            }
            instance = this;    
        }

        akBullets = new List<Bullet>(initialPoolSize);
        fireLines = new List<LineRenderer>(initialPoolSize);
        hitImages = new List<SpriteRenderer>(initialPoolSize);
        for (int i = 0; i < initialPoolSize; i++)
        {
            GameObject prefabInstance = Instantiate(bulletPrefab);
            prefabInstance.transform.SetParent(transform);
            prefabInstance.SetActive(false);
            akBullets.Add(prefabInstance.GetComponent<Bullet>());
        }
        for (int i = 0; i < initialPoolSize; ++i)
        {
            LineRenderer prefabInstance = Instantiate(firelinePrefab).GetComponent<LineRenderer>();
            prefabInstance.transform.SetParent(transform);
            prefabInstance.enabled = false;
            fireLines.Add(prefabInstance.GetComponent<LineRenderer>());
        }
        for(int i = 0; i < initialPoolSize; ++i)
        {
            SpriteRenderer prefabInstance = Instantiate(hitImagePrefab).GetComponent<SpriteRenderer>();
            prefabInstance.transform.SetParent(transform);
            prefabInstance.enabled = false;
            hitImages.Add(prefabInstance);
        }
        for(int i = 0; i < initialPoolSize; ++i)
        {
            ParticleSystem prefabInstance = Instantiate(ps_Blood).GetComponent<ParticleSystem>();
            prefabInstance.transform.SetParent(this.transform);
            bloodEffect.Add(prefabInstance);
        }
        for (int i = 0; i < initialPoolSize; ++i)
        {
            ParticleSystem prefabInstance = Instantiate(ps_Sparks).GetComponent<ParticleSystem>();
            prefabInstance.transform.SetParent(this.transform);            
            sparksEffects.Add(prefabInstance);
        }
    }

   public Bullet GetBullet()
    {
        foreach (Bullet bullet in akBullets)
        {
            if (!bullet.gameObject.activeInHierarchy)
            {
                bullet.gameObject.SetActive(true);                
                return bullet; 
            }
        }

        GameObject newBullet = Instantiate(bulletPrefab, transform);
        var newBulletImpact = newBullet.GetComponent<Bullet>();
        akBullets.Add(newBulletImpact);
        return newBulletImpact; 
    }

    public void TriggerFIreLine(Vector3 origin, RaycastHit destination)
    {
        if(destination.collider.tag == "Enemy")
        {
            StartCoroutine(DrawFireLine_enemy(origin, destination));
        }
        else
        {
            StartCoroutine(DrawFireLine_wall(origin, destination));
            if(destination.collider.gameObject.layer == 10)
            {
                
            }
        }
    }

    IEnumerator DrawFireLine_wall(Vector3 origin, RaycastHit destination)
    {
        #region Draw Fire Line
        if (fireLines.Count == 0)
        {
            LineRenderer prefabInstance = Instantiate(firelinePrefab).GetComponent<LineRenderer>();
            prefabInstance.transform.SetParent(transform);
            prefabInstance.enabled = false;
            fireLines.Add(prefabInstance.GetComponent<LineRenderer>());
        }
        var firelineToSet = fireLines[0];
        firelineToSet.enabled = true;
        fireLines.RemoveAt(0);
        firelineToSet.SetPosition(0, origin);
        firelineToSet.SetPosition(1, destination.point);
        #endregion
        #region Set Bullet Hole Image
        if (hitImages.Count == 0)
        {
            SpriteRenderer prefabInstance = Instantiate(hitImagePrefab).GetComponent<SpriteRenderer>();
            prefabInstance.transform.SetParent(transform);
            prefabInstance.enabled = false;
            hitImages.Add(prefabInstance);
        }
        var hitImageToSet = hitImages[0];
        hitImages.RemoveAt(0);
        hitImageToSet.enabled = true;
        hitImageToSet.transform.parent = null;
        hitImageToSet.transform.position = destination.point;        
        hitImageToSet.transform.rotation = Quaternion.LookRotation(-destination.normal);
        #endregion
        #region Set Sparks Particle System
        if (sparksEffects.Count == 0)
        {
            ParticleSystem prefabInstance = Instantiate(ps_Sparks).GetComponent<ParticleSystem>();
            prefabInstance.transform.SetParent(this.transform);            
            sparksEffects.Add(prefabInstance);            
        }
        var sparkEffect = sparksEffects[0];
        sparksEffects.RemoveAt(0);
        sparkEffect.transform.parent = null;
        sparkEffect.transform.position = destination.point;
        sparkEffect.transform.rotation = Quaternion.LookRotation(-destination.normal);        
        sparkEffect.Play();
        #endregion

        yield return new WaitForSeconds(firelineDrawDuration);
        firelineToSet.transform.parent = this.transform;
        firelineToSet.enabled = false;
        fireLines.Add(firelineToSet);      
        
        yield return new WaitForSeconds(hitImageDuration - firelineDrawDuration);
        hitImageToSet.transform.parent = transform;
        hitImageToSet.enabled = false;
        hitImages.Add(hitImageToSet);
        
        sparkEffect.Stop();
        sparkEffect.transform.parent = transform;
        sparksEffects.Add(sparkEffect);
    }
    IEnumerator DrawFireLine_enemy(Vector3 origin, RaycastHit destination)
    {
        if (fireLines.Count == 0)
        {
            LineRenderer prefabInstance = Instantiate(firelinePrefab).GetComponent<LineRenderer>();
            prefabInstance.transform.SetParent(transform);
            prefabInstance.enabled = false;
            fireLines.Add(prefabInstance.GetComponent<LineRenderer>());
        }
        if (bloodEffect.Count == 0)
        {
            ParticleSystem prefabInstance = Instantiate(ps_Blood).GetComponent<ParticleSystem>();
            prefabInstance.transform.SetParent(this.transform);
            prefabInstance.Stop();
            bloodEffect.Add(prefabInstance);
        }
        var firelineToSet = fireLines[0];
        var blood = bloodEffect[0];
        fireLines.RemoveAt(0);
        bloodEffect.RemoveAt(0);
        blood.transform.parent = null;
        blood.transform.position = destination.point;
        blood.transform.rotation = Quaternion.LookRotation(destination.normal);
        blood.Stop();
        blood.Play();
        firelineToSet.SetPosition(0, origin);
        firelineToSet.SetPosition(1, destination.point);
        firelineToSet.enabled = true;
        yield return new WaitForSeconds(firelineDrawDuration);
        firelineToSet.enabled = false;
        fireLines.Add(firelineToSet);
        yield return new WaitUntil(()=> blood.isStopped);       
        bloodEffect.Add(blood);
    }
}
