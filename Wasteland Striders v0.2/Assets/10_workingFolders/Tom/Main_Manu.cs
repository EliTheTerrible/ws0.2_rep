﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Main_Manu : MonoBehaviour
{
    public void Start_Game()
    {
        SceneManager.LoadSceneAsync(1);

    }
    public void Exit()
    {
#if UNITY_EDITOR

        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
    public void go_menu()
    {
        SceneManager.LoadSceneAsync(0);

    }
    public void load1()
    {
        SceneManager.LoadSceneAsync(2);

    }
    public void load2()
    {
        SceneManager.LoadSceneAsync(3);

    }
    public void load3()
    {
        SceneManager.LoadSceneAsync(4);

    }
    public void reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);


    }
}

